{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "my-project"
, dependencies = [ "console", "effect", "psci-support", "maybe", "foreign-object", "redux", "record-extra", "typelevel-prelude" ]
, packages = ./packages.dhall
}
